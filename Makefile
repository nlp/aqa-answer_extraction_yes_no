data: mkdata test train eval

mkdata:
	mkdir data

test:
	./make_train_data.py \
	 --out data \
     --db_path /nlp/projekty/question_answering/AQA_v2/sqad_tools/sqad2database_devel/sqad_db/devel/v3_2_16-09-2022_16-34-35/v3_2_16-09-2022_16-34-35_base \
     -s parts.dump \
     -p $@

train:
	./make_train_data.py \
	 --out data \
     --db_path /nlp/projekty/question_answering/AQA_v2/sqad_tools/sqad2database_devel/sqad_db/devel/v3_2_16-09-2022_16-34-35/v3_2_16-09-2022_16-34-35_base \
     -s parts.dump \
     -p $@

eval:
	./make_train_data.py \
	 --out data \
     --db_path /nlp/projekty/question_answering/AQA_v2/sqad_tools/sqad2database_devel/sqad_db/devel/v3_2_16-09-2022_16-34-35/v3_2_16-09-2022_16-34-35_base \
     -s parts.dump \
     -p $@

finetunning:
	python3 ./fine_tunning.py --db_path /nlp/projekty/question_answering/AQA_v2/sqad_tools/sqad2database_devel/sqad_db/devel/v3_2_16-09-2022_16-34-35/v3_2_16-09-2022_16-34-35_base \

extraction_file:
	./answer_extraction.py -m train_2022_08_12_18-59-18/xlm-roberta-large-squad2-finetuned-V1 --input_file data/test --eval_with_db --url '0.0.0.0' --port 9001 -o ./test_2022_08_12_18-59-18/

extraction_stdin:
	./answer_extraction.py -i "je tom hanks americký filmový herec ?|thomas jeffrey \" tom \" hanks ( * 9 . července 1956 concord ) je americký filmový herec , režisér a producent , držitel dvou oscarů za herecký výkon ." -m train_2022_09_19_17-38-32/xlm-roberta-large-squad2-finetuned-V1/

clean:
	rm -rf *.json

run_service:
	./ml_answer_extraction_server_yes_no.py --port 9004 --model_path ./train_2022_09_19_17-38-32/xlm-roberta-large-squad2-finetuned-V1/