#!/usr/bin/env python3
# coding: utf-8
# Author: Marek Medved

import json
import os
import sys
import logging
from transformers import pipeline
from transformers import AutoTokenizer
from transformers import AutoModelForSequenceClassification
from http.server import HTTPServer, BaseHTTPRequestHandler
current_dir = os.path.dirname(os.path.realpath(__file__))

classify = None
id2label = {'LABEL_0': 'no', 'LABEL_1': 'yes'}


class JobServer(BaseHTTPRequestHandler):
    def _set_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.end_headers()

    def _get_data(self):
        content_length = int(self.headers['Content-Length'])
        return json.loads(self.rfile.read(content_length))

    def do_POST(self):
        command = self.path[1:]
        data = self._get_data()

        query_input = {
            'text': f'{data["question"]} [SEP] {data["answer_selection"]}'
        }

        prediction = classify(query_input)

        logging.info(f'POST {command}; {data["question"]}, {data["answer_selection"]}')
        self._set_headers()
        self.wfile.write(json.dumps({'command': command,
                                     'question': data['question'],
                                     'answer_selection': data["answer_selection"],
                                     'answer_extraction': id2label[prediction["label"]],
                                     'answer_extraction_score': prediction["score"]}).encode('utf-8'))


def main():
    import argparse
    parser = argparse.ArgumentParser(description='Fasttext word embedding service')
    parser.add_argument('--port', type=int,
                        required=True, default=9003,
                        help='Port')
    parser.add_argument("--model_path",
                        help="Specifies model path", required=True)
    args = parser.parse_args()

    global classify
    tokenizer = AutoTokenizer.from_pretrained(args.model_path)
    tokenizer.padding_side = "right"
    model = AutoModelForSequenceClassification.from_pretrained(args.model_path)
    classify = pipeline('text-classification', model=model, tokenizer=tokenizer)

    logging.basicConfig(filename=f'{current_dir}/server.log',
                        filemode='a',
                        format='%(asctime)s|%(levelname)s %(message)s',
                        datefmt='%H:%M:%S',
                        level=logging.DEBUG)

    logging.info(f'Starting httpd on port {args.port}...')
    httpd = HTTPServer(('localhost', args.port), JobServer)
    sys.stderr.write(f'Running Answer extraction service (AE-ML-YES_NO) on port {args.port}\n')
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()
    logging.info('Stopping httpd...\n')


if __name__ == "__main__":
    main()
