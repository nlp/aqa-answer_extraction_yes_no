#!/usr/bin/env python3
# coding: utf-8
# Author: Marek Medved, marek.medved@sketchengine.eu, Lexical Computing CZ
import sys
import pickle
import json
from sqad_db import SqadDb
from query_database import get_record


def get_record_parts(rid, db):
    """
    Get sentence representation form database
    :param rid: str, record id
    :param db: sqad_db object, connection to database
    :return: str, str, str
    """
    sys.stderr.write(f'processing: {rid}\n')
    vocabulary, _, kb = db.get_dicts()
    record = get_record(db, rid, word_parts='w')

    if record['a_type'] == 'YES_NO':

        if record['yes_no_answer'] == 'yes':
            yes_no_answer = 1
        elif record['yes_no_answer'] == 'no':
            yes_no_answer = 0
        else:
            sys.stderr.write(f'ERROR ({rid}): incorrect YES/NO answer')
        sys.stderr.write(f'answer: {record["yes_no_answer"]} {yes_no_answer}\n')
        question_content = ''
        for question_sent in record['question']:
            for token in question_sent['sent']:
                question_content += f'{token["word"]} '.lower()

        correct_answer_content = ''
        for answer_selection_sent in record['a_sel']:
            for token in answer_selection_sent['sent']:
                correct_answer_content += f'{token["word"]} '.lower()

        return question_content, correct_answer_content, yes_no_answer
    else:
        return '', '', None


def process(data, db_path, out_f, part):
    """
    Create ech part separatly
    :param data: list od record ids
    :param db_path: str, path to database
    :param out_f: FD, output fle
    :param part: str, name of part
    :return: None
    """
    print(f'Creating {part} part')

    output = {'data': [], 'version': 'sqad_3_v1'}

    for rid in data[part]:
        question_content, correct_answer_content, yes_no_answer = get_record_parts(rid, db_path)

        if yes_no_answer != None:
            output['data'].append({'id': rid,
                                   'label': yes_no_answer,
                                   'text': f'{question_content} [SEP] {correct_answer_content}',
            })

    with open(f'{out_f}/{part}', 'w', encoding='utf-8') as f:
        json.dump(output, f, indent=4)


def main():
    import argparse
    parser = argparse.ArgumentParser(description='Create train, test amd eval data for answer extraction')
    parser.add_argument('-o', '--out', type=str,
                        required=True,
                        help='Output path')
    parser.add_argument('--db_path', type=str,
                        required=False,
                        help='Database path')
    parser.add_argument('--url', type=str,
                        required=False, default='',
                        help='Database URL')
    parser.add_argument('--port', type=int,
                        required=False, default=None,
                        help='Server port')
    parser.add_argument('-s', '--split_dump', type=str,
                        required=True,
                        help='Dump with records devided into dev, eval and train part')
    parser.add_argument('-p', '--part', type=str,
                        required=False, default='all',
                        help='Prt to create: train, test, eval, all')
    args = parser.parse_args()

    # Filling the record ids into parts
    sys.stderr.write('Splitting records.\n')
    with open(args.split_dump, 'rb') as fd:
        split_dump = pickle.load(fd)

    db = None
    if (args.url and args.port) or args.db_path:
        if args.url and args.port:
            db = SqadDb(url=args.url, port=args.port, read_only=True)
        elif args.db_path:
            db = SqadDb(file_name=args.db_path, read_only=True)
    else:
        sys.stderr.write('Please specify --db_path or (--port and --url)')
        sys.exit()

    # Creating content of each part
    if args.part in ['train', 'all']:
        sys.stderr.write('Train part ...\n')
        process(split_dump, db, args.out, 'train')
    if args.part in ['test', 'all']:
        sys.stderr.write('Test part ...\n')
        process(split_dump, db, args.out, 'test')
    if args.part in ['eval', 'all']:
        sys.stderr.write('Eval part ...\n')
        process(split_dump, db, args.out, 'eval')


if __name__ == "__main__":
    main()
