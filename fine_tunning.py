#!/usr/bin/env python3
# coding: utf-8
# Author: Marek Medved, marek.medved@sketchengine.eu, Lexical Computing CZ
# source: https://www.kaggle.com/code/bhavikardeshna/fine-tuning-xlm-roberta-question-answering/notebook
# model: https://huggingface.co/deepset/xlm-roberta-large-squad2

import os
import sys
import datasets
import logging
from sqad_db import SqadDb
from query_database import get_record
from config_train import Config
from transformers import AutoTokenizer
from transformers import PreTrainedTokenizerFast
from transformers import AutoModelForSequenceClassification
from transformers import TrainingArguments
from transformers import Trainer
from transformers import DataCollatorWithPadding

conf = Config()


def get_record_parts(rid, db):
    """
    Get sentence representation form database
    :param rid: str, record id
    :param db: sqad_db object, connection to database
    :return: str, str, str
    """
    vocabulary, _, kb = db.get_dicts()
    record = get_record(db, rid, word_parts='w')

    question_content = ''
    for question_sent in record['question']:
        for token in question_sent['sent']:
            question_content += f'{token["word"]} '

    correct_answer_content = ''
    for answer_selection_sent in record['a_sel']:
        for token in answer_selection_sent['sent']:
            correct_answer_content += f'{token["word"]} '

    return question_content, correct_answer_content, record['yes_no_answer'], record['q_type'], record['a_type']


def preprocess_function(examples, tokenizer):
    return tokenizer(examples["text"], truncation=True)


def main():
    import argparse
    parser = argparse.ArgumentParser(description='Fine tunning of xml roberta')
    parser.add_argument('--db_path', type=str,
                        required=False,
                        help='Database path')
    parser.add_argument('--url', type=str,
                        required=False, default='',
                        help='Database URL')
    parser.add_argument('--port', type=int,
                        required=False, default=None,
                        help='Server port')
    parser.add_argument('--notes', type=str,
                        required=False, default='',
                        help='Notes')
    args = parser.parse_args()

    if not os.path.exists(conf.output_dir):
        os.makedirs(conf.output_dir)

    id2label = {0: 'no', 1: 'yes'}
    # =========================================
    # Logging
    logging.basicConfig(filename=f'{conf.output_dir}/training.log',
                        filemode='a',
                        format='%(message)s',
                        level=logging.INFO)

    # =========================================
    # DB access
    db = None
    if (args.url and args.port) or args.db_path:
        if args.url and args.port:
            db = SqadDb(url=args.url, port=args.port, read_only=True)
        elif args.db_path:
            db = SqadDb(file_name=args.db_path, read_only=True)
    else:
        sys.stderr.write('Please specify --db_path or (--port and --url)')
        sys.exit()

    # =========================================
    # Loading training data
    czech_train_path = f'{conf.train_data}/train'
    train_dataset = datasets.load_dataset(
        'json',
        data_files=czech_train_path,
        field='data',
        split="train"
    )

    czech_test_path = f'{conf.train_data}/test'
    test_dataset = datasets.load_dataset(
        'json',
        data_files=czech_test_path,
        field='data',
        split="train"
    )

    czech_eval_path = f'{conf.train_data}/eval'
    eval_dataset = datasets.load_dataset(
        'json',
        data_files=czech_eval_path,
        field='data',
        split="train"
    )

    czech_dataset = datasets.dataset_dict.DatasetDict()
    czech_dataset['train'] = train_dataset
    czech_dataset['eval'] = eval_dataset

    logging.info('czech_dataset')
    logging.info(czech_dataset)

    # =========================================

    tokenizer = AutoTokenizer.from_pretrained(conf.model_path)
    tokenizer.padding_side = "right"
    assert isinstance(tokenizer, PreTrainedTokenizerFast)

    tokenized_czech = czech_dataset.map(
        preprocess_function,
        fn_kwargs={"tokenizer": tokenizer},
        batched=True,
        remove_columns=['id', 'text']
    )

    data_collator = DataCollatorWithPadding(tokenizer=tokenizer)

    model = AutoModelForSequenceClassification.from_pretrained(conf.model_path, num_labels=2)

    # https://huggingface.co/docs/transformers/v4.21.1/en/main_classes/trainer#transformers.TrainingArguments
    training_args = TrainingArguments(output_dir=conf.output_dir,
                                      learning_rate=conf.learning_rate,
                                      per_device_train_batch_size=conf.batch_size,
                                      per_device_eval_batch_size=conf.batch_size,
                                      num_train_epochs=conf.num_train_epochs,
                                      weight_decay=conf.weight_decay)

    trainer = Trainer(model=model,
                      args=training_args,
                      train_dataset=tokenized_czech["train"],
                      eval_dataset=tokenized_czech["eval"],
                      tokenizer=tokenizer,
                      data_collator=data_collator)

    trainer.train()

    validation_features = test_dataset.map(
        preprocess_function,
        fn_kwargs={"tokenizer": tokenizer},
        batched=True,
        remove_columns=['id', 'text']
    )

    raw_predictions = trainer.predict(validation_features)

    metric = datasets.load_metric("accuracy")
    # Original answers
    references = [ex["label"] for ex in test_dataset]
    # Scores
    scores = metric.compute(predictions=raw_predictions.label_ids, references=references)

    print(scores)
    logging.info(scores)

    example_index_to_id = {i: k for i, k in enumerate(test_dataset["id"])}

    match = 0
    un_match = 0
    total_num = 0
    os.mkdir(f'{conf.output_dir}/test_results/')

    for idx, predicted_label in enumerate(raw_predictions.label_ids):
        total_num += 1
        question, answer_sentence, orig_answer, q_type, a_type = get_record_parts(example_index_to_id[idx], db)
        with open(f'{conf.output_dir}/test_results/{example_index_to_id[idx]}', 'w') as f:

            f.write(f'Record: {example_index_to_id[idx]}\n')
            f.write(f'Question: {question}\n')
            f.write(f'Type: q:{q_type} a:{a_type}\n')
            f.write(f'Answer selection: {answer_sentence}\n')
            f.write(f'Org answer: {orig_answer}\n')
            f.write(f'New answer: {id2label[predicted_label]}\n')

            if orig_answer == id2label[predicted_label]:
                match += 1
                f.write(f'Match\n')
            else:
                un_match += 1
                f.write(f'Unmatch\n')

    with open(f'{conf.output_dir}/test_results/full_results.txt', 'w') as f:
        f.write('Final results:\n')
        f.write(f'Match: {match} | {(match*100)/total_num}%\n')
        f.write(f'Unmatch: {un_match} | {(un_match*100)/total_num}%\n')
        f.write(f'Accuracy: {scores}\n')

    scores["description"] = ''
    scores['length train'] = len(czech_dataset['train'])
    scores['length eval'] = len(czech_dataset['eval'])
    scores['length test'] = len(test_dataset)

    training_output = f"{conf.output_dir}/{conf.model_name}-finetuned-{conf.version}"

    # Save model
    trainer.save_model(training_output)

    # Save training parameters
    conf.notes = args.notes
    conf.save_config(f'{training_output}.json')


if __name__ == '__main__':
    main()
